# simc_gfortran - Hall C/A Physics Monte Carlo

SIMC (simc_gfortran) is the standard Hall C Monte Carlo for
coincidence reactions, written in FORTRAN.

## Features


* SIMC simulates the optics (using COSY models) and apertures of the
  Hall C spectrometers (HMS, SOS, SHMS) and other spectrometers at
  [Jefferson Lab](http://www.jlab.org/) (HRS's, BigCal, ...)
* Radiative effects, multiple scattering, ionization energy loss and
  particle decay are included
* Simple presecriptions are available for Final State Interactions,
  Coulomb Corrections and other effects.
* This is a modified version of simc with the following additions:
  1. Capable of reading events from an input file. ***This is currently the
     the only mode that is supported***
  2. CMAKE based build system
  3. Launcher that allows the simc program to be invoked from any directory

## Building This Version

### On your own machine
1. Ensure you have the dependencies installed:
    1. gfortran
    2. python3
2. Pick a directory where you would like to build simc, e.g. $HOME/build, go to
   this directory.
3. Clone the simc-file-input repository
```bash
git clone --recurse-submodules git@gitlab.com:jpsi007/simc-file-input.git
cd simc-file-input
mkdir build  && cd build
```
3. Decide where you would like to install the package, e.g. /usr/local or 
   $HOME/stow/simc_file_input
4. Point the SIMC_PREFIX environment variable to this install directory.
```bash
export SIMC_PREFIX="<YOUR_INSTALL_DIRECTORY_HERE>"
```
5. Run cmake and make:
```bash
cmake ../. -DCMAKE_INSTALL_PREFIX=$SIMC_PREFIX
make -j4 install
```

### On the farm
1. Use bash
```bash
bash
```
2. Enable the module system
```bash
source /etc/profile.d/modules.sh
source /group/c-csv/local/setup.sh
```
3. Load the dependencies
```bash
module load gcc/latest
module load cmake/latest
export PATH=/apps/python/3.4.3/bin/:$PATH
```
2. Pick a directory where you would like to build simc, e.g. in your work 
   directory$HOME/build, go to this directory.
3. Clone the simc-file-input repository
```bash
git clone --recurse-submodules git@gitlab.com:jpsi007/simc-file-input.git
cd simc-file-input
mkdir build  && cd build
```
3. Decide where you would like to install the package, e.g 
   $HOME/stow/simc_file_input
4. Point the SIMC_PREFIX environment variable to this install directory.
```bash
export SIMC_PREFIX="<YOUR_INSTALL_DIRECTORY_HERE>"
```
5. Run cmake and make:
```bash
cmake ../. -DCMAKE_INSTALL_PREFIX=$SIMC_PREFIX
make -j4 install
```

## Event file format
simc reads in your events from a plain text file. The format for this file is
one line per event where the following HMS and SHMS variables are given.
Note that at this point, we don't pass the vertex information yet **TODO**.
```bash
hms_xptar hms_yptar hms_particle_mom shms_xptar shms_yptar shms_particle_mom weight
```
Definitions:
```
hms_* :  HMS particle
shms_*:  SHMS particle
*_xptar: dx/dz relative to the central ray
*_yptar: dy/dz relative to the central ray
*_particle_momentum: Particle momentum in MeV
weight: event weight
```
The coordinate system is at the target center with z pointing along the central
spectrometer angle, +x pointing vertical down and +y pointing to the left
(i.e. smaller HMS angles and larger SHMS angles).

## Tutorial
A simple example of how to use simc is installed under 
$SIMC_PREFIX/share/simc/examples.

1. (If needed:) Add the install bin directory to your PATH. For bash this would be:
```bash
export PATH=${SIMC_PREFIX}/bin:$PATH
```
2. Create an output directory for your example, e.g. /tmp/example. Point
   an environment variable to this directory so we can easily refer to it in
   this tutorial
```bash
export SIMC_TUTORIAL_DIR="<YOUR_TUTORIAL_DIR>"
```
3. Look at the simc help using the -h flag
```bash
simc -h
```
4. To run simc, you will need a configuration file (input file) and an event
   list. The input file defines the spectrometer settings, while the event file
   is a text that uses the format as described above. You can find the files
   for this example in $SIMC_PREFIX/share/simc/examples
   1. The configuration file is called example1.inp. You pass it to simc with
      the required -c flag
   2. The event list is called sample_events.dat
5. simc will need to know where to write its output files. You tell simce by
   passing the required -o flag.
6. Lets run simc!
```bash
simc -c $SIMC_PREFIX/share/simc/examples/example1.inp \
     -o $SIMC_TUTORIAL_DIR \
     $SIMC_PREFIX/share/simc/examples/sample_events.dat
```
7. This writes a couple of output files to your output directory. Let's go
   there and check it out!
```bash
cd $SIMC_TUTORIAL_DIR; ls
```
8. The two most important files are:
    1. example1.log: The log file containing the log output from simc
    2. example1.rzdat: An HBOOK file with the simc output. You can convert this
                       to the ROOT format using the h2root program that comes
                       with ROOT.
9. That's all!

# Old simc documentation

*You probably won't need the information past this point*.

## Reactions

***At this time, this version of simc only supports running from an external
   event list. Running with the internal generator will be enabled in the
   launcher soon.***
   
SIMC has physics models for the following reactions.
* Elastic and quasi-elastic scatering: H(e,e'p), A(e,e'p)
* Exclusive pion production: H(e,e'pi+)n, A(e,e'pi+/-)
(quasifree or coherent)
* Kaon electroproduction: H(e,e'K+)Lambda,Sigma, A(e,e'K+/-),A(e,e'K-)
* Semi-inclusive pion production: H(e,e'pi+/-)X, D(e,e'pi+/-)X
* Semi-inclusive kaon production: H(e,e'K+/-)X, D(e,e'K+/-)X
* Diffractive rho production: H(e,e'rho->pi+ pi-)p, D(e,e'rho->pi+
pi-)

## SIMC is NOT

* Not a full detector response simulation a la GEANT/GEANT4
* Does NOT simulate a large class of processes simultaneously to
gerate backgrounds (like Pythia for example)
* Not a generic event generator.  Processes are generated over a
limited phase space matching the spectrometer acceptances
* Not hard to modify

## Overview

An overview of SIMC can be found in
[this presentation at the 2009 Hall A Collaboration
Meeting](http://hallaweb.jlab.org/collab/meeting/2009-winter/talks/Analysis%20Workshop%20--%20Dec%2014/simc_overview.pdf)

## Documentation

For more information, see the [SIMC Monte Carlo page in the Hall C
Wiki](https://hallcweb.jlab.org/wiki/index.php/SIMC_Monte_Carlo)